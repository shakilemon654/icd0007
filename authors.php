<?php
print_r($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<nav class="navbar">
    <h3><a id="book-list-link" href="index.php">Books</a></h3>
    <h3><a id="book-form-link" href="add-book.php">Add book</a></h3>
    <h3><a id="author-list-link" href="authors.php">Authors</a></h3>
    <h3><a id="author-form-link" href="add-author.php">Add author</a></h3>
</nav>
<div>
    <table class="list-table">
        <thead>
        <tr>
            <th scope="col">First name</th>
            <th scope="col">Surname</th>
            <th scope="col">Grade</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Faith</td>
            <td>Mehmed</td>
            <td>5</td>
        </tr>

        </tbody>
    </table>
</div>
<footer>
    <h3>ICD007: Authors list</h3>
</footer>
</body>
</html>

<?php

$authors = [];
$authors['firstName'] = $_GET['firstName'];
$authors['lastName'] = $_GET['lastName'];
$authors['grade'] = $_GET['grade'];

$string = print_r($authors, true);
file_put_contents('authors.txt', $string, FILE_APPEND);

?>