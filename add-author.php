<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<nav class="navbar">
    <h3><a id="book-list-link" href="index.php">Books</a></h3>
    <h3><a id="book-form-link" href="add-book.php">Add book</a></h3>
    <h3><a id="author-list-link" href="authors.php">Authors</a></h3>
    <h3><a id="author-form-link" href="add-author.php">Add author</a></h3>
</nav>
<div class="form-div">
    <form action="authors.php" method="GET">
        <table>
            <tr>
                <td><label for="firstName"><h3>First name:</h3></label></td>
                <td><input class="form-control" type="text" name="firstName" id="firstName" placeholder="Enter author first name"  size="60" required></td>
            </tr>
            <tr>
                <td><label for="lastName"><h3>Surname:</h3></label></td>
                <td><input class="form-control" type="text" name="lastName" id="lastName" placeholder="Enter author surname"  size="60" required></td>
            </tr>
            <tr>
                <td><h3>Grade:</h3></td>
                <td>
                    <input type="radio" name="grade" value="1" required>
                    <label for="">1</label>
                    <input type="radio" name="grade" value="2">
                    <label for="">2</label>
                    <input type="radio" name="grade" value="3">
                    <label for="">3</label>
                    <input type="radio" name="grade" value="4">
                    <label for="">4</label>
                    <input type="radio" name="grade" value="5">
                    <label for="">5</label>
                </td>
            </tr>
        </table>
        <button type="submit" name="submitButton" value="add-author" class="submit-button">Submit</button>
    </form>
</div>
<footer>
    <h3>ICD007: Add new author</h3>
</footer>
</body>
</html>